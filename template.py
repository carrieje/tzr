from pdffiller import Template

__all__ = ['abs_template', 'issr_template', 'transport_template']

# Define Absence template and its fields

abs_template = Template('templates/demande-absence.pdf')
abs_template.add_field('nom-prenom', page=1, x=167, y=663, fontSize=12)
abs_template.add_field('qualite', page=1, x=167, y=635, fontSize=12)
abs_template.add_field('date absence', page=1, x=282, y=592, fontSize=12)
abs_template.add_field('nombre jours', page=1, x=165, y=565, fontSize=12)
abs_template.add_field('nombre heures', page=1, x=395, y=565, fontSize=12)
abs_template.add_field('motif_sante', page=1, x=103, y=531, fontSize=16)
abs_template.add_field('motif_enfant_malade', page=1, x=103, y=514, fontSize=16)
abs_template.add_field('motif_examen', page=1, x=103, y=497, fontSize=16)
abs_template.add_field('motif_reunion_syndicale', page=1, x=103, y=480, fontSize=16)
abs_template.add_field('motif_autre', page=1, x=103, y=463, fontSize=16)
abs_template.add_field('motif_autre_extra', page=1, x=217, y=450, fontSize=12)
abs_template.add_field('today', page=1, x=459, y=420, fontSize=12)

# Define ISSR template and its fields

issr_template = Template('templates/issr-request.pdf')
issr_template.add_field('gender_male', page=1, x=99, y=675, fontSize=10)
issr_template.add_field('gender_female', page=1, x=64, y=675, fontSize=10)
aligned_field = lambda key, y : issr_template.add_field(key, page=1, x=170,
                                                        y=y, fontSize=12)
aligned_field('last name', 652),
aligned_field('first name', 629),
aligned_field('INSEE', 606),
aligned_field('corps', 581),
aligned_field('discipline', 558),
aligned_field('assignment', 534),
issr_template.add_field('month_year', page=1, x=240, y=485, fontSize=12)
issr_template.add_field('month_year', page=1, x=240, y=462, fontSize=12)
issr_template.add_field('absences', page=1, x=385, y=458, fontSize=14)

# Define Transport template and its fields

transport_template = Template('templates/transport.pdf')

transport_template.add_field('last name', page=1, x=115, y=490, fontSize=12)
transport_template.add_field('first name', page=1, x=300, y=490, fontSize=12)
transport_template.add_field('INSEE', page=1, x=135, y=476, fontSize=12)
transport_template.add_field('assignment', page=1, x=135, y=463, fontSize=12)
transport_template.add_field('grade', page=1, x=135, y=451, fontSize=12)
transport_template.add_field('discipline', page=1, x=135, y=438, fontSize=12)
transport_template.add_field('periode', page=1, x=200, y=383, fontSize=12)
aligned_field = lambda key, y : transport_template.add_field(key, page=1, x=133,
                                                        y=y, fontSize=12)
aligned_field('personal-address-street', 302)
aligned_field('personal-address-city', 289)
aligned_field('personal-address-zipcode', 275)
aligned_field('work-address-street', 234)
aligned_field('work-address-city', 221)
aligned_field('work-address-zipcode', 207)

transport_template.add_field('from', page=1, x=176, y=163, fontSize=12)
transport_template.add_field('to', page=1, x=176, y=149, fontSize=12)

def transport_field(index, y):
    key = 'transport-{}'.format(str(index))
    transport_template.add_field(key, page=1, x=85, y=y, fontSize=12)
    transport_template.add_field(key + '-cost', page=1, x=268, y=y, fontSize=12)

transport_field(index=1, y=78)
transport_field(index=2, y=64)
transport_field(index=3, y=50)
transport_field(index=4, y=36)

transport_template.add_field('annuel', page=1, x=440, y=511, fontSize=12)
transport_template.add_field('mensuel-ill', page=1, x=440, y=476, fontSize=12)
transport_template.add_field('mensuel-lim', page=1, x=440, y=452, fontSize=12)
transport_template.add_field('hebdo', page=1, x=440, y=413, fontSize=12)
transport_template.add_field('total', page=1, x=610, y=388, fontSize=12)

transport_template.add_field('date', page=1, x=590, y=215, fontSize=12)
