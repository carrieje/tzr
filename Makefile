.PHONY: current

DAY=$(shell date '+%d')
MONTH=$(shell date '+%m')
YEAR=$(shell date '+%Y')
NAME=$(shell date -jf '%m-%d' ${MONTH}-01 '+%B')

OUT=out/${YEAR}–${MONTH}–${DAY} Demande ISSR ${NAME} ${YEAR}.pdf

current:
	@pipenv run python tzr issr -m ${MONTH} -y ${YEAR} -a templates/arrets.pdf templates/timetable.pdf -o '${OUT}'

