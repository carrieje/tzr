from datetime import datetime

persona = {
    'INSEE': '0 00 00 00 000 000 00', # Implies gender
    'first name': 'John',
    'last name': 'Doe',
    'corps': 'Certifié',
    'grade': '0',
    'discipline': 'Mathématiques',
    'assignment': 'Collège de là-bas',
    'work-address-street': '7 baker street',
    'work-address-city': 'Grenoble',
    'work-address-zipcode': '38000',
    'personal-address-street': '7 baker street',
    'personal-address-city': 'Grenoble',
    'personal-address-zipcode': '38000',
}

transport = {
    'from': 'Home Station',
    'to': 'Work Station',
    #'annuel': 'x',
    #'mensuel-lim': 'x',
    'mensuel-ill': 'x',
    #'hebdo': 'x',
    'transport-1': 'TER',
    'transport-1-cost': '40€',
    'transport-2': 'TAG',
    'transport-2-cost': '40€',
    #'transport-3': 'TAG',
    #'transport-3-cost': '40€',
    #'transport-4': 'TAG',
    #'transport-4-cost': '40€',
    'total': '145,34',
}

absences = [
    # Septembre
    '2021/09/23', # Grève
]

absences = [datetime.strptime(date, '%Y/%m/%d') for date in absences]
