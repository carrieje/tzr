# What does it do?

For now, only ISSR request generation.

# How to use it?

1. install the virtual environment `pipenv install`

2. Fill the `data.py` file, especially the `persona` dict.

3. Presign the `templates/issr-request.pdf` file and replace
`templates/arrets.pdf` and `templates/timetable.pdf` with your own files.

4. Run `make` or `make YEAR=2022 MONTH=02`

5. Enjoy opening the request generated in the `out/` directory.

# Show me a quick example

Simply run `make` and open the file generated under the `out/` directory.
